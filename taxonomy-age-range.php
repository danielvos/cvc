<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="content">
	<div class="inner">
		<div class="text-box">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->
			<?php
					echo '<ul class="programs_list">';

			
			// Start the Loop.
			while ( have_posts() ) : the_post(); 

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
	 
						// Start the Loop ?>
 
						<li class="grade" id="post-<?php the_ID(); ?>">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php 
							if (get_field('science_sols')) {
								echo '<p><strong>Science SOLs:</strong> ' . get_field('science_sols') . '</p>';
							}
							echo get_the_term_list( $post->ID, 'age-range', '<p><strong>Grade:</strong> ', ', ', '</p>' ); 
							the_excerpt();
							echo '<a href="' . get_the_permalink() . '" class="button">Learn More</a> ';
							$program = get_the_title();?>
							<a href="<?php echo esc_url( add_query_arg( "the_program", $program, site_url( "/request-outreach-program/" ) ) )?>" class="button">Schedule this Program</a>
							
						</li>
		 
	 

			<?php // End the loop.
			endwhile;
					echo '</ul>';
	 				echo '<br>';

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</div>
	</div>
</div>

<?php get_footer(); ?>
