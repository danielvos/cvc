<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php twentysixteen_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->

		<?php 
			if (is_page('classroom-programs')){
				$terms = get_terms( 'age-range', array(
					'orderby'    => 'count',
					'hide_empty' => 1,
					'parent' => 0
				) );
				// now run a query for each animal family
				foreach( $terms as $term ) {
 
					// Define the query
					$args = array(
						'post_type' => 'program',
						'age-range' => $term->slug,
						'order'		=> ASC
					);
					$query = new WP_Query( $args );
			 
					// output the term name in a heading tag                
					echo'<h2>' . $term->name . '</h2>';
	 
					// output the post titles in a list
					echo '<ul class="programs_list">';
	 
						// Start the Loop
						while ( $query->have_posts() ) : $query->the_post(); ?>
 
						<li class="grade" id="post-<?php the_ID(); ?>">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php 
							echo get_the_term_list( $post->ID, 'age-range', 'Grade: ', ', ' ); 
							the_excerpt();
							 ?>
						</li>
		 
						<?php endwhile;
	 
					echo '</ul>';
	 				echo '<br>';
					// use reset postdata to restore orginal query
					wp_reset_postdata();
 
				}
			}
		?>
