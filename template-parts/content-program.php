<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();
			
			the_taxonomies('template=<p><strong>%s:</strong> %l.</p>'); 		
			if (get_field('science_sols')) {
				echo '<p><strong>Science SOLs:</strong> ' . get_field('science_sols') . '</p>';
			}
			if (get_field('concepts_introduced')) {
				echo '<p><strong>Concepts and Vocabulary Introduced</strong> ' . get_field('concepts_introduced') . '</p>';
			}
			if (get_field('equipment_needed')) {
				echo '<p><strong>Equipment Needed:</strong> ' . get_field('equipment_needed') . '</p>';
			}
			if (get_field('duration')) {
				echo '<p><strong>Duration:</strong> ' . get_field('duration') . '</p>';
			}
			if (get_field('support_materials')) {
if( have_rows('support_materials') ): ?>
<p><strong>Support Materials</strong></p>
	<ul class="material">
 	<?php // loop through the rows of data
    while ( have_rows('support_materials') ) : the_row();

        // display a sub field value
       echo '<li><a href="' . get_sub_field('files') . '" target="_blank">' . get_sub_field('title') . '</a></li>';

 endwhile;?>

	</ul>
<?php else :

    // no rows found

endif;

			}
			
			$program = get_the_title(); ?>
			<a href="<?php echo esc_url( add_query_arg( "the_program", $program, site_url( "/request-outreach-program/" ) ) )?>" class="button">Schedule this Program</a>

			
			<?php wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
