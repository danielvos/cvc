<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		<div id="footer">
			<div class="inner">
				<div class="fourth">
					<?php dynamic_sidebar( 'footer-widget-1' ); ?>
				</div>
				<div class="fourth">
					<?php dynamic_sidebar( 'footer-widget-2' ); ?>
				</div>
				<div class="fourth">
					<?php dynamic_sidebar( 'footer-widget-3' ); ?>
				</div>				
				<div class="fourth">
					<?php dynamic_sidebar( 'footer-widget-4' ); ?>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "http://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=120813844667038";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-page" data-href="https://www.facebook.com/CleanValleyCouncil/" data-small-header="true" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/CleanValleyCouncil/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/CleanValleyCouncil/">Clean Valley Council</a></blockquote></div>
				</div>
				<div style="clear: both;"></div>
			</div>
		</div>
	<script>
		window.onload = function() {
		  document.getElementById("reCycle").focus();
		};
	</script>


<?php wp_footer(); ?>
</body>
</html>
