<?php

get_header(); ?>
		<?php 
				// check if the repeater field has rows of data
		if( have_rows('features') ):

			// loop through the rows of data
			while ( have_rows('features') ) : the_row();
		$image = get_sub_field('image');
		$size = 'feature'; // (thumbnail, medium, large, full or custom size)

		echo '<div class="panel">';
		if( $image ) { echo wp_get_attachment_image( $image, $size ); }
		echo '<div class="text-box">';
		echo '	<h2>' . get_sub_field('headline') . '</h2>';
		echo '	<p>' . get_sub_field('subhead') . '</p>';
		echo '	<a href="' . get_sub_field('button_link') . '" class="button">' . get_sub_field('button_text') . '</a>';
		echo '</div>';
		echo '</div>';

			endwhile;

		else :

			// no rows found

		endif;
		
		?>

<?php get_footer(); ?>
